package br.com.witzler.clientes.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/usuarioscolaboradores")
@RequiredArgsConstructor
public class UsuariosColaboradoresController {
//	private final PasswordEncoder passwordEncoder;
//	private final JwtService jwtService;
//	private final UsuariosColaboradoresServiceImpl usuarioService;
//	private final UsuariosColaboradoresRepository repository;

//	@PostMapping("criarUsuarioColaborador")
//	@ResponseStatus(HttpStatus.CREATED)
//	public UsuariosColaboradores salvar(@RequestBody @Valid UsuariosColaboradores usuario) {
//		String senhaCriptografada = passwordEncoder.encode(usuario.getPassword_colaborador());
//		usuario.setPassword_colaborador(senhaCriptografada);
//		return usuarioService.salvar(usuario);
//	}

//	@PutMapping("/updatecolaborador/{id_colaborador")
//	public UsuariosColaboradores update(@RequestBody @Valid UsuariosColaboradores usuario, Integer id_colaborador) {
//		String senhaCriptografada = passwordEncoder.encode(usuario.getPassword_colaborador());
//		usuario.setPassword_colaborador(senhaCriptografada);
//		return usuarioService.salvar(usuario);
//	}
//
//	@DeleteMapping("/elimina-colaborador/{username}")
//	@ResponseStatus(HttpStatus.NO_CONTENT)
//	public void deletar(@PathVariable String username) {
//		repository.findByUsername(username).map(usuario -> {
//			repository.delete(usuario);
//			return Void.TYPE;
//		}).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//	}
//	
//	@PutMapping("/novasenha-colaborador/{username}")
//	public UsuariosColaboradores update(@RequestBody @Valid UsuariosColaboradores usuario, String username) {
//		String senhaCriptografada = passwordEncoder.encode(usuario.getPassword_colaborador());
//		usuario.setPassword_colaborador(senhaCriptografada);
//		return usuarioService.salvar(usuario);
//	}
//	
//	@GetMapping("/info/usuario-colaborador/{username}")
//	public List<UsuariosColaboradores> listaInfoUsuario(@PathVariable String username) {
//		List<UsuariosColaboradores> usuario = usuarioService.listandoInfoUsuarioColaborador(username);
//		return usuario;
//	}
//	
//	@GetMapping("/info/clientes-colaborador/{colaborador_tec_id}")
//	public List<UsuariosColaboradores> listaClientesUsuario(@PathVariable int colaborador_tec_id) {
//		List<UsuariosColaboradores> usuario = usuarioService.listandoClientesUsuarioColaborador(colaborador_tec_id);
//		return usuario;
//	}
//
//	@PostMapping("/auth-colaborador")
//	public TokenDTO autenticar(@RequestBody CredenciaisColaboradorDTO credenciais) {
//		try {
//			UsuariosColaboradores usuario = UsuariosColaboradores.builder()
//					.id_colaborador(credenciais.getId_colaborador()).username(credenciais.getUsername())
//					.password_colaborador(credenciais.getPassword_colaborador()).build();
//
//			UserDetails usuarioAutenticado = usuarioService.autenticarColaborador(usuario);
//			String token = jwtService.gerarTokenColaborador(usuario);
//			return new TokenDTO(usuario.getId_colaborador(), usuario.getUsername(), token);
//
//		} catch (UsernameNotFoundException | SenhaInvalidaException e) {
//			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//		}
//	}
}

package br.com.witzler.clientes.rotine;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.Objects;


@Component
public class Rotine {
//    private final String urlAnaliseDiretorios = "C:\\Witzler\\testes_assets\\";
    private final String urlAnaliseDiretorios = File.separator+"var"+File.separator+"www"+File.separator+"webWitzler1.0"+File.separator+"backend"+File.separator+"assets"+File.separator+"files"+File.separator+"clientes"+File.separator;

    @Scheduled(fixedRate = 600000)
    public void executeRotine(){ // DE DEZ EM DEZ MINUTOS
        System.out.println("## ATIVANDO GARBAGE COLLECTOR DE ARQUIVOS DESCRIPTOGRAFADOS ##");
        System.out.println("ESTOU RODANDO NO: " + System.getProperty("os.name"));
        if(!System.getProperty("os.name").toLowerCase().contains("windows")){
            //LocalDate localDate = LocalDate.now();
            //if(localDate.getDayOfWeek() == DayOfWeek.THURSDAY){
//            LocalTime hora = LocalTime.now();
//            System.out.println(hora.getHour());
//            System.out.println(hora.getMinute());
//            if(hora.getHour() == 22 ){ // && hora.getMinute() == 28 // hora
                verificarDiretorios();
//            }
            //}
        }

    }

    private void verificarDiretorios(){
        try{
            File diretorio = new File(this.urlAnaliseDiretorios);
            for (File pasta : diretorio.listFiles()){
                if(!pasta.isDirectory()){
                    continue;
                }
                // System.out.println(pasta.getName());
                try{
                    if(Long.parseLong(pasta.getName()) != 0){
                        for(File pastaIdUnidade : Objects.requireNonNull(pasta.listFiles())){
                            if(pastaIdUnidade.getName().toLowerCase().contains("arquivos")){
                                for(File pastaFaturaEnergia : Objects.requireNonNull(pastaIdUnidade.listFiles())){
                                    if(pastaFaturaEnergia.getName().toLowerCase().contains("fatura_energia")){
                                        for(File arquivoAntesDataFaturaEnergia : Objects.requireNonNull(pastaFaturaEnergia.listFiles())){
                                            if(arquivoAntesDataFaturaEnergia.getName().toLowerCase().contains("-")){
                                                for(File pastaComArquivosCriptografados : Objects.requireNonNull(arquivoAntesDataFaturaEnergia.listFiles())){
                                                    //System.out.println(pastaComArquivosCriptografados.getAbsolutePath());
                                                    if(pastaComArquivosCriptografados.getName().toLowerCase().contains(".decrypt")){
                                                        System.out.println("## DELETAR " + pastaComArquivosCriptografados.getAbsolutePath() + " ##");
                                                        deletarArquivo(pastaComArquivosCriptografados);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch(Exception ex2){ // PASTA QUE NÃO É NUMERO
                    if(pasta.getName().toLowerCase().contains("unidades")){
                        for(File arquivoDentroPastaUnidades : Objects.requireNonNull(pasta.listFiles())){ // PASTAS DENTRO DE UNIDADES
                            for(File pastaArquivosDentroDeUnidades : Objects.requireNonNull(arquivoDentroPastaUnidades.listFiles())){
                                if(pastaArquivosDentroDeUnidades.getName().toLowerCase().contains("arquivos")){
                                    for(File arquivoDentroPastaArquivosDentroDeUnidades : Objects.requireNonNull(pastaArquivosDentroDeUnidades.listFiles())){
                                        if(
                                                arquivoDentroPastaArquivosDentroDeUnidades.getName().toLowerCase().contains("fatura_distribuidora")
                                                        ||
                                                arquivoDentroPastaArquivosDentroDeUnidades.getName().toLowerCase().contains("relatorios")
                                                        ||
                                                arquivoDentroPastaArquivosDentroDeUnidades.getName().toLowerCase().contains("boletos")
                                        ){
                                            for(File arquivoDentroPastaRelatoriosOuFaturasDistribuidoras : Objects.requireNonNull(arquivoDentroPastaArquivosDentroDeUnidades.listFiles())){
                                                if(arquivoDentroPastaRelatoriosOuFaturasDistribuidoras.getName().contains("-")){
                                                    for(File arquivoCriptografado : Objects.requireNonNull(arquivoDentroPastaRelatoriosOuFaturasDistribuidoras.listFiles())){
                                                        if(arquivoCriptografado.getName().toLowerCase().contains(".decrypt")){
                                                            System.out.println("## DELETAR " + arquivoCriptografado.getAbsolutePath() + " ##");
                                                            deletarArquivo(arquivoCriptografado);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }catch(Exception ex){
            System.out.println("Erro ao montar este diretório: " + ex);
        }

    }

    private void deletarArquivo(File arquivoParaSerDeletado){
        try{
            Files.delete(Paths.get(arquivoParaSerDeletado.getAbsolutePath()));
            Thread.sleep(3000);
            System.out.println("DELETADO COM SUCESSO!");
        }catch(Exception ex){
            System.out.println("Opa, aconteceu algum problema ao deletar o seguinte arquivo: " + arquivoParaSerDeletado.getAbsolutePath());
            System.out.println("## VERIFIQUE POR FAVOR ##");
            System.exit(0);
        }
    }

}